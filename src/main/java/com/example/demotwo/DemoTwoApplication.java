package com.example.demotwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoTwoApplication {

	@GetMapping("/welcome")
	public String welcome() {
		return "Spring boot demo";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoTwoApplication.class, args);
	}

}
