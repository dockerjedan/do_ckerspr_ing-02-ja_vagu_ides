# define base docker image
FROM openjdk:17
LABEL maintainer="blabla"
ADD target/demo-two-0.0.1-SNAPSHOT.jar demo-two-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "demo-two-0.0.1-SNAPSHOT.jar"]